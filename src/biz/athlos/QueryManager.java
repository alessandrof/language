package biz.athlos;
import java.sql.*;
import java.sql.DriverManager;


public class QueryManager {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    static final String host = "localhost";
    static final String DB_URL = "jdbc:mysql://" + host + ":3306" + "/" + "DB_parole";

	public static Connection getConnessione() {
		
		Connection conn= null;
		
		try {
			
			 // Registro il driver JDBC
            Class.forName("com.mysql.jdbc.Driver");
			// Effettuo la connessione
			conn = DriverManager.getConnection(DB_URL,"root","");
		}catch(ClassNotFoundException cnfx){
			cnfx.printStackTrace();
		}catch(SQLException  sqlx)
		{
			sqlx.printStackTrace();
		}
		
		return conn;
	}

	public static String cercaNelDB(String parola, Connection conn) {
		String query = "SELECT * from parola WHERE parola LIKE(?)";
		String result = "";
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(query);
			stmt.setString(1, parola);
			
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				result = rs.getString("lang");
		
			}
		}
		catch (SQLException sqlx) {
			sqlx.printStackTrace();
		}
		
		return result;
	}

}
