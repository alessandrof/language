package biz.athlos;
import java.sql.*;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Utilizza la classe Scanner di java.util
		int n=10;
		System.out.println("Inserisci una stringa: \n");
		Scanner input = new Scanner(System.in);
		String s = input.nextLine();
		System.out.println("Ho letto: " + s);
	//	String[] arrayfrase = new String[n];
		
		ArrayList<String> fraseSplit = new ArrayList(Arrays.asList(s.split(" ")));		
		
		fraseSplit.forEach(parola -> System.out.println(parola)); //
		
		final Connection conn = QueryManager.getConnessione();		
		ArrayList<String> risultati = new ArrayList<>();
		
		risultati = new ArrayList(fraseSplit.parallelStream()
							  				.map(parola -> QueryManager.cercaNelDB(parola, conn))
							  				.collect(Collectors.toList()));		
				
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		int paroleItaliane=0;
		int paroleInglesi=0;
		float percentualeIt=0;
		
		for(int k=0; k< risultati.size(); k++){
			if(risultati.get(k).equals("IT")) paroleItaliane++;
			else if(risultati.get(k).equals("EN")) paroleInglesi++;			
		}
		
//		percentualeIt = risultati.parallelStream()
//								 .filter(ris -> ris.equals("it"))
//								 .count();
		
		percentualeIt = (paroleItaliane / risultati.size())*100; 
		
		if (percentualeIt > 70) System.out.println("Frase Italiana");
		else System.out.println("Frase Inglese");

	}
		
		
		
	}


